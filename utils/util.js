//时间格式化
function formatTime(date) {
  if (!date) {
    date = new Date();
  }

  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds();


  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}


function networkStatus() {
  wx.getNetworkType({
    success: function(res) {
      if (res.errMsg !== 'getNetworkType:ok') {
        wx.showModal({
          content: '获取网络状态失败',
          showCancel: false
        })
        return false;
      }
      if (res.networkType === 'none') {
        wx.showModal({
          content: '当前网络不可用，请检查网络设置',
          showCancel: false
        })
        return false;
      }
    }
  })
}

module.exports = {
  networkStatus: networkStatus
}

//验证码画布
module.exports = class Mcaptcha {
  constructor(options) {
    this.options = options;
    this.fontSize = options.height * 3 / 4;
    this.init();
    this.refresh(this.options.code);
  }
  init() {
    this.ctx = wx.createCanvasContext(this.options.el);
    this.ctx.setTextBaseline("middle");
    this.ctx.setFillStyle(this.randomColor(180, 240));
    this.ctx.fillRect(0, 0, this.options.width, this.options.height);
  }
  refresh(code) {
    let arr = (code + '').split('');
    if (arr.length === 0) {
      arr = ['e', 'r', 'r', 'o', 'r'];
    };
    let offsetLeft = this.options.width * 0.6 / (arr.length - 1);
    let marginLeft = this.options.width * 0.2;
    arr.forEach((item, index) => {
      this.ctx.setFillStyle(this.randomColor(0, 180));
      let size = this.randomNum(24, this.fontSize);
      this.ctx.setFontSize(size);
      let dis = offsetLeft * index + marginLeft - size * 0.3;
      let deg = this.randomNum(-30, 30);
      this.ctx.translate(dis, this.options.height * 0.5);
      this.ctx.rotate(deg * Math.PI / 180);
      this.ctx.fillText(item, 0, 0);
      this.ctx.rotate(-deg * Math.PI / 180);
      this.ctx.translate(-dis, -this.options.height * 0.5);
    })
    this.ctx.draw();
  }
  randomNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min);

  }
  randomColor(min, max) {
    let r = this.randomNum(min, max);
    let g = this.randomNum(min, max);
    let b = this.randomNum(min, max);
    return "rgb(" + r + "," + g + "," + b + ")";
  }

}