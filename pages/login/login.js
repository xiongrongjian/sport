  /**
   *时间： 2018.10.14
   *作者： 熊荣健制作
   *描述： 登录页面
   */


//"pages/login/login",制作者请注意  登录必须放app.json中

  //获取组件工具
  let Util = require('../../utils/util.js');
  //获取全局变量
  const app = getApp();

var url = app.appData.url//获取地址
  Page({

    /**
     * 页面的初始数据
     */
    //首先定义为账户密码空值
    data: {
      username: null,
      password: null,
      //验证码
      code: null,
      inputCode: null
    },

    //学号、密码、验证码绑定
    inputUsername: function(e) {
      this.setData({
        username: e.detail.value
      })
    },
    inputPassword: function(e) {
      this.setData({
        password: e.detail.value
      })
    },
    inputCode: function(e) {
      this.setData({
        inputCode: e.detail.value
      })
    },




    //小程序进入执行第一个逻辑函数
    //程序生命周期只执行一次
    onLoad: function() {
      // 删除测试，发布时记得删除
      // wx.removeStorage({
      //   key: 'student_info_account_password',
      //   success: function(res) {},
      // })


  

     
      var that=this;
      //缓存中是否有登录的key
      wx.getStorage({
        key: 'student_info_account_password',
        //如果有
        success: function(res) {
          //赋给全局用户信息
          app.appData.student_info = res.data
          
          //首页的跳转用Switch，普通页面不能用Switch,用navigator
          wx.switchTab({
            url: '../index/index',
          })
          //停止代码执行login
         return ;
         
        },
        fail: function() {    
          
        }
      })


    },

    //缓存中是没有登录的key
    //用户登录
    login: function() {
      var username = this.data.username;
      var password = this.data.password;
      var inputCode = this.data.inputCode;
      var code = this.data.code;
      if (username == null || username == "" | password == "" || password == null) {
        wx.showToast({
          title: '不准为空',
          image: "/image/error.png"
        })
        //验证码更新
        this.onReady();
        //账户密码为空不正确 不执行下面代码
        return;
      }
      //账户密码不为空执行下面代码

      //验证码判断
      if (inputCode === code || inputCode === code.toUpperCase() || inputCode === code.toLowerCase()) {
        //验证码正确
        //显示加载框
        wx.showLoading({
          title: '请求中',
        })
        var that = this;
        wx.request({
          url: url+"/validate_student_account",
          //向服务器传入的数据
          data: {
            username: username,
            password: password
          },
          //Post请求
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },


          //连接服务器成功 
          success: function(res) {
            //账户密码正确    
            if (res.data.code ==200) {
              setTimeout(function() {   
                wx.hideLoading();       
              }, 100)
              //设置app.js里的student_info全局设置
              app.appData.student_info = res.data.data;
              //保持账户到缓存异步
              wx.setStorage({
                key: 'student_info_account_password',
                data: res.data.data,
              })
            
              //跳转到tarTab的首页面用switchTab
              wx.switchTab({
                url: '/pages/index/index',
              })
            }

            //密码错误 
            else {
              //隐藏加载框
              wx.hideLoading();
              setTimeout(function() {
                wx.showToast({
                  title: '密码错误',
                  image: "/image/error.png",
                  duration: 2000
                });
              }, 100)
              //更新验证码
              that.onReady();
            }
          },

          //连接服务器失败
          fail: function() {
            //隐藏加载框
            wx.hideLoading();
            setTimeout(function() {
              wx.showToast({
                title: '连接失败',
                image: "/image/error.png",
                duration: 2000
              })
            }, 100)
            //更新验证码
            that.onReady();
          },

          //超过15秒未有反应，请求失败，隐藏加载框
          complete: function() {
            setTimeout(function() {
              wx.hideLoading();
            }, 15000)
          }
        })
      } 

      //验证码不正确
      else {
        wx.showToast({
          title: '验证码不正确',
          image: "/image/error.png"
        })
        //更新验证码
        this.onReady();
        //不正确 停止
        return;
      }
    },



    /**
     * 刷新验证码的函数
     */
    onReady: function() {
      var that = this;
      var num = that.getRanNum();
      this.setData({
        num: num
      })
      new Util({
        el: 'canvas',
        width: 100, //对图形的宽高进行控制
        height: 46,
        code: num
      });
    },
    getRanNum: function() {
      var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
      var code = '';
      for (var i = 0; i < 4; i++) {
        if (Math.random() < 48) {
          code += chars.charAt(Math.random() * 48 - 1);
        }
      }

      this.setData({
        code: code
      })
      return code;
    }


  })