 /**
  *时间： 2018.10.16
  *作者： 熊荣健制作
  *描述： 首页页面
  */

 var Charts = require('./../../utils/charts');
 const app = getApp();
 var url = app.appData.url //获取地址

 Page({

   /**
    * 页面的初始数据
    */
   data: {

     area: [],
     begin: 0, //资讯分页
     end: 10, //资讯分页
     list: [], //资讯信息

     todayGoal: 0, //今日完成 公里
     todayComplete: 0, //今日完成 公里
     percentToday: 0, //今日完成与今日目标的比值

     termGoal: 0, //学期目标 公里
     termComplete: 0, //学期完成 公里
     percentTerm: 0 //学期完成与学期目标的比值
   },

   /**
    * 生命周期函数--监听页面加载 
    */
   onLoad: function(options) {


     
     var that=this;
     //获取学生的目标
     wx.request({
       url: url + '/get_student_goal',
       data: {
         student_id:app.appData.student_info.id  //通过学生的id获取其目标
       },
       success: function(res) {
         if (res.data.code == 200) {
           var todayGoal = res.data.todayGoal;
           var todayComplete = res.data.todayComplete;
           var termGoal = res.data.termGoal;
           var termComplete = res.data.termComplete;
           var area = res.data.area.reverse(); //数组倒序

           var percentToday = ((todayComplete / todayGoal) * 100).toFixed(0);

           if (percentToday > 100) {
             percentToday = 100
           }
           var percentTerm = ((termComplete / termGoal) * 100).toFixed(0);
           if (percentTerm > 100) {
             percentTerm = 100
           }
           that.setData({
             todayGoal: todayGoal,
             todayComplete: todayComplete,
             termGoal: termGoal,
             termComplete: termComplete,
             area: area
           });

           that.setData({

             percentToday: percentToday,
             percentTerm: percentTerm
           });

          

           //柱状图============================================
           new Charts({
             type: 'bar',
             data: that.data.area,
             bgColors: "#46a2ef",
             color: '#383838',
             cHeight: 135, //表格高度
             cWidth: 500, //表格宽度
             bWidth: 20, //柱子宽度
             bMargin: 25, //柱子间距
             showYAxis: true, //是否显示Y轴
             xCaption: '近7日记录(天)',
             yCaption: '运动公里数（KM）',
             canvasId: 'chartContainer'
           });
           //柱状图============================================

           //圆图跑步============================

           var todayGoal = that.data.todayGoal;
           var todayComplete = that.data.todayComplete;
           var cxt_arc = wx.createCanvasContext('canvasArc'); //创建并返回绘图上下文context对象。 
           cxt_arc.setLineWidth(2);
           cxt_arc.setStrokeStyle('#d2d2d2');
           cxt_arc.setLineCap('round')
           cxt_arc.beginPath(); //开始一个新的路径 

           cxt_arc.arc(70, 70, 42, 0, 2 * Math.PI, false); //设置一个原点(106,106)，半径为30的圆的路径到当前路径 //注意了这里的原点相加等于页面的 130px的空间大小
           cxt_arc.stroke(); //对当前路径进行描边 

           if (todayComplete !== null && todayGoal !== null) {
             cxt_arc.setLineWidth(3);
             cxt_arc.setStrokeStyle('#DB7093');
             cxt_arc.setLineCap('round')
             cxt_arc.beginPath(); //开始一个新的路径 todayComplete

             cxt_arc.arc(70, 70, 48, 0, 2 * Math.PI * Number(todayComplete) / Number(todayGoal), false); //画图百分比
             cxt_arc.stroke(); //对当前路径进行描边 
           }

           cxt_arc.draw();
           //圆图跑步============================


           //圆图强度=================================
           var cxt_arc2 = wx.createCanvasContext('canvasArcs2'); //创建并返回绘图上下文context对象。 
           cxt_arc2.setLineWidth(2);
           cxt_arc2.setStrokeStyle('#d2d2d2');
           cxt_arc2.setLineCap('round')
           cxt_arc2.beginPath(); //开始一个新的路径 
           cxt_arc2.arc(70, 70, 42, 0, 2 * Math.PI, false); //设置一个原点(106,106)，半径为30的圆的路径到当前路径 //注意了这里的原点相加等于页面的 130px的空间大小
           cxt_arc2.stroke(); //对当前路径进行描边 

           if (todayComplete !== null && todayGoal !== null) {
             cxt_arc2.setLineWidth(3);
             cxt_arc2.setStrokeStyle('#FF8C00');
             cxt_arc2.setLineCap('round')
             cxt_arc2.beginPath(); //开始一个新的路径 todayComplete
             cxt_arc2.arc(70, 70, 48, 0, 2 * Math.PI * Number(todayComplete) / Number(todayGoal), false); //画图百分比
             cxt_arc2.stroke(); //对当前路径进行描边 
           }

           cxt_arc2.draw();
           //圆图强度============================


           var termGoal = that.data.termGoal;
           var termComplete = that.data.termComplete;
           //直线=================================
           var CanvasLine1 = wx.createCanvasContext('CanvasLine1')
           CanvasLine1.setFillStyle("#d2d2d2")
           CanvasLine1.fillRect(0, 0, 40, 5) //第三个比例，宽度，第一二个位置比
           CanvasLine1.setFillStyle("#1E90FF")
           CanvasLine1.fillRect(0, 0, todayComplete / todayGoal * 40, 5)
           CanvasLine1.stroke()
           CanvasLine1.draw()



           var CanvasLine3 = wx.createCanvasContext('CanvasLine3')
           CanvasLine3.setFillStyle("#d2d2d2")
           CanvasLine3.fillRect(0, 0, 40, 5) //第三个比例，宽度，第一二个位置比
           CanvasLine3.setFillStyle("#1E90FF")
           CanvasLine3.fillRect(0, 0, termComplete / termGoal * 40, 5)
           CanvasLine3.stroke()
           CanvasLine3.draw()
     //直线=================================
     // 设置总体和当前目标百分比

         } else {
           wx.showToast({
             title: '系统错误',
             icon: "none"
           })
           return;
         }
       },
       fail: function(res) {
         wx.showToast({
           title: '连接不上服务器',
           icon: "none",
           duration: 3000
         })
         return;
       }
     })
 
   },

   beginSport: function() {
     //这里必须先判断下学生在redis是否有未完成的，若有则进入，若无则不准进入
     //当学生一结束，则返回到首页
     var that = this;
     wx.request({
       url: url + '/select_is_sport',
       data: {
         studentId: app.appData.student_info.id //学生的id
       },
       success: function(res) {
         if (res.data.code == 200) { //表示有，可以进入
           wx.navigateTo({
             url: 'sport/sport',
           })
           return;

         } else { //表示没有，进行时间的判断

           var hours = new Date().getHours(); //获取当前的小时
           if (hours == 23) {
             wx.showModal({
               title: '提醒',
               content: '夜深了，睡觉吧，不要运动了',
               showCancel: false
             })
           } else if (hours >= 23 && hours <= 23) {
             wx.showModal({
               title: '提醒',
               content: '系统维护，6点后再来',
               showCancel: false
             })
           } else if (hours >= 11 && hours <= 12) {
             wx.showModal({
               title: '提醒',
               content: '好好午休哦，不要运动了',
               showCancel: false
             })
           } else {
             wx.navigateTo({
               url: 'sport/sport',
             })
           }

           return;
         }
       },
       fail: function(res) {
         wx.showToast({
           title: '连接不上服务器',
           icon: "none",
           duration: 3000
         })
         return;
       }
     })

   },

   changeData: function() { //再type页面返回到index页面，进行刷新操作
     this.onLoad(); //最好是只写需要刷新的区域的代码，onload也可，效率低，有点low
   },


   /**
     * 生命周期函数--监听页面初次渲染完成
     */
   onReady: function () {},

   
   onReachBottom() {
     //上拉获取资讯
     wx.showLoading({
       title: '玩命加载中',
       icon: "loading",
       mask: true //此作用防止触摸
     })

     var that = this;
     var begin = that.data.begin;
     var end = that.data.end;
     wx.request({
       url: url + '/get_part_information',
       data: {
         begin: begin,
         end: end
       },
       success: function(res) {
         var list = that.data.list;
         if (res.data.length == 0) {
           wx.showToast({
             title: '已经没有啦',
             icon: "none"
           })
           return;
         }

         for (var i = 0; i < res.data.length; i++) {
           list.push(res.data[i]);
         }
         that.setData({
           list: list,
           begin: begin + 11,
           end: end + 11
         });


         setTimeout(function() {
           wx.hideLoading(); //0.1秒钟后隐藏
         }, 100)
       },
       fail: function() {
         wx.showToast({
           title: '连接不上服务器',
           icon: "none",
           duration: 3000
         })
         return;
       }
     })


   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function() {
     wx.getStorage({
       key: 'student_info_account_password',
       //如果有
       success: function (res) {
     
       },
       fail: function () {//没有用户的数据重新登录
         wx.redirectTo({
           url: '../login/login',
         })

       }
     })
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function() {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function() {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function() {
      this.onLoad();
     setTimeout(function() {
       // 隐藏导航栏加载框
       wx.hideNavigationBarLoading();
       // 停止下拉动作
       wx.stopPullDownRefresh();
     }, 2000);

   },



   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function() {

   }
 })