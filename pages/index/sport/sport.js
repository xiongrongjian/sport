 /**
   *时间： 2018.11.4
   *作者： 熊荣健制作
   *描述： 运动页面
   */
const app = getApp();
var url = app.appData.url; //获取地址
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    placeId:0
  },

  sport: function(e) {
    //此处先查下redis是否有已经 的运动记录,若有则提示
    var that = this;
    wx.request({
      url: url + '/select_is_sport_return_student_begin_time',
      data: {
        studentId: app.appData.student_info.id //学生的id
      },
      success: function(res) {
        if (res.data.code == 200) { //表示已经有记录了
        that.setData({
          placeId: res.data.placeId
        })
          if (e.currentTarget.dataset.id != res.data.placeId) {
            //判断已有的记录是否跟点击的运动一致，不一致提示
            wx.showModal({
              title: "警告",
              content: res.data.message,
              showCancel: false
            })
            return;


          } else { //一致则跳转
            wx.navigateTo({ //这里只能是navigateTo，否则返回首页不刷新
              url: 'type?sportType=' + res.data.sportType + "&placeId=" + res.data.placeId + "&existUnfinishSport=" + true + "&latitudeSmall=" + e.currentTarget.dataset.latitudesmall + "&latitudeBig=" + e.currentTarget.dataset.latitudebig + "&longitudeSmall=" + e.currentTarget.dataset.longitudesmall + "&longitudeBig=" + e.currentTarget.dataset.longitudebig + "&placeName=" + e.currentTarget.dataset.placename + "&timeSmall=" + res.data.timeSmall + "&timeBig=" + res.data.timeBig + "&timeSmallShow=" + res.data.timeSmallShow+
                "&timeBigShow=" + res.data.timeBigShow, //跳转的这个页面只能返回到首页,这里再传送一个值，表示有记录了，按钮为结束
         
              //然后那个页面根据开始和结束去执行任务
            })
          }
        } else { //表示没有记录，直接进入
          wx.redirectTo({ //这里只能是redirectTo，避免学生多个运动开始但是一直不返回首页将不刷新，弊端，需要学生手动下拉刷新
            url: 'type?sportType=' + e.currentTarget.dataset.sporttype + "&placeId=" + e.currentTarget.dataset.id + "&existUnfinishSport=" + false + "&latitudeSmall=" + e.currentTarget.dataset.latitudesmall + "&latitudeBig=" + e.currentTarget.dataset.latitudebig + "&longitudeSmall=" + e.currentTarget.dataset.longitudesmall + "&longitudeBig=" + e.currentTarget.dataset.longitudebig + "&placeName=" + e.currentTarget.dataset.placename, //跳转的这个页面只能返回到首页,这里再传送一个值，表示无记录了，按钮默认为开始，然后那个页面根据开始和结束去执行任务
          })
          return;

        }
      },
      fail: function(res) {
        wx.showToast({
          title: '连接不上服务器',
          icon: "none",
          duration: 3000
        })
        return;
      }
    })


  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    //获取地点地址
    wx.request({
      url: url + '/get_place',

      success: function(res) {
        if (res.data.code == 200) {
          var place = res.data.data;
          that.setData({
            list: place
          })

        } else {
          wx.showToast({
            title: '你代码有误',
            icon: "none"
          })
          return;
        }
      },
      fail: function(res) {
        wx.showToast({
          title: '连接不上服务器',
          icon: "none",
          duration: 3000
        })
        return;
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})