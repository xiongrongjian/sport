 /**
  *时间： 2018.11.4
  *作者： 熊荣健制作
  *描述： 运动类型页面
  */
 const app = getApp();
 var url = app.appData.url; //获取地址
 Page({

       /**
        * 页面的初始数据
        */
       data: {
         items: [],
         startX: 0, //开始坐标
         startY: 0,
         placeId: 0, //地点的id
         placeName: null, //地点的名字
         existUnfinishSport: false,
         latitudeBig: null,
         latitudeSmall: null,
         longitudeBig: null,
         longitudeSmall: null,
         latitude: null,
         longitude: null,
         timeSmall: null,
         timeBig: null,
         timeSmallShow:null,
         timeBigShow:null
       },



       //结束任务
       end: function() {
         var existUnfinishSport = this.data.existUnfinishSport;
         var latitudeBig = this.data.latitudeBig;
         var latitudeSmall = this.data.latitudeSmall;
         var longitudeBig = this.data.longitudeBig;
         var longitudeSmall = this.data.longitudeSmall;
         var placeName = this.data.placeName;
         var that = this;
         //这里进行判断地点是否合格


         //对用户进行时间上的提示
         var timeSmall = new Date(that.data.timeSmall.replace(/\-/g, "\/")) //时间格式转换
         var timeCurrent = new Date();
         if (timeCurrent > timeSmall) {


           wx.showModal({
               title: "提醒",
               content: "是否结束本次签到",
               success: function(res) {


                 if (res.confirm) {
                   that.getLocation();
                   var latitude = that.data.latitude;
                   var longitude = that.data.longitude;
                   //这里进行判断地点是否合格
                   if (latitude != null && longitude!=null&&(latitude >= latitudeSmall && latitude <= latitudeBig) && (longitude >= longitudeSmall && longitude <= longitudeBig)) {
                     wx.showLoading({
                       title: '正在结束',
                       mask: true
                     })
                     wx.request({
                       url: url + "/insert_sportdetail_update_termcomplete",
                       data: {
                         studentId: app.appData.student_info.id, //学生的id
                         placeId: that.data.placeId //地点
                       },
                       success: function (res) {
                         var message = res.data.message
                         wx.hideLoading();
                         if (res.data.code == 200) {
                           that.changeParentData(); //返回首页之前进行刷新
                    
                    
                    
                    //此处最好有个返回的分数结果给学生查看
                    
                    
                    
                    
                           wx.showToast({
                             title: message,
                             icon: "none",
                             duration: 2000,
                             mask: true //禁止触摸屏幕
                           })
                           that.setData({
                             existUnfinishSport: !existUnfinishSport
                           })

                           setTimeout(function () {

                             wx.switchTab({
                               url: '../index',
                             })
                           }, 1800)


                         } else {
                           wx.showModal({
                             title: "提示",
                             content: message, //这里写错误的提示
                             showCancel: false,
                             success: function (res) {
                               if (res.confirm) {
                                 wx.switchTab({
                                   url: '../index',
                                 })
                               }
                             }
                           })
                           return;
                         }
                       },
                       fail: function (res) {
                         wx.hideLoading();
                         wx.showToast({
                           title: '连接不上服务器',
                           icon: "none",
                           duration: 3000
                         })
                         return;
                       }
                     })

                   } else {
                     wx.showModal({
                       title: 'GPS警告',
                       content: "请前往" + placeName + "地点在结束运动",
                       showCancel: false
                     })
                     return;
                   }


                 }

               }
               })


           }
           else {
             wx.showModal({
               title: '警告',
               content: "签到时间不足半小时，现在结束不保存记录，你确定结束？",
               success: function(res) {
                 if (res.confirm) {
                   wx.showLoading({
                     title: '正在结束',
                     mask: true
                   })
                   wx.request({
                     url: url + "/delete_is_sport",
                     data: {
                       studentId: app.appData.student_info.id //学生的id
                     },
                     success: function(res) {
                       wx.hideLoading();
                       if (res.data.code == 200) {
                         wx.showToast({
                           title: '取消成功',
                           icon: "none"
                         })
                         setTimeout(function() {
                           wx.switchTab({
                             url: '../index',
                           })
                         }, 1000)

                       } else {
                         wx.showToast({
                           title: '系统出错了',
                           icon: "none"
                         })
                       }
                     },
                     fail: function(res) {
                       wx.hideLoading();
                       wx.showToast({
                         title: '连接不上服务器',
                         icon: "none",
                         duration: 3000
                       })
                       return;
                     }
                   })
                 }
               }
             })
           }

         },


         getLocation: function() { //这里获取本手机的GPS
             var that = this;
             wx.getLocation({
               type: 'gcj02',
               success: function(res) {
                 that.setData({
                   latitude: res.latitude,
                   longitude: res.longitude
                 })

                },
               fail: function() {
                 wx.showModal({
                   title: "GPS警告",
                   content: "检查不到GPS,请到个人中心设置开启GPS权限",
                   showCancel: false,
                   success: function(res) {
                     if (res.confirm) {
                       wx.redirectTo({
                         url: '../../person/setting/setting', //这个页面是跳到GPS开启权限的页面
                       })
                     }
                    }
                 })
               }
             })
           },

           //开始任务
           begin: function(e) {
             var existUnfinishSport = this.data.existUnfinishSport;
             this.getLocation();
             //这里进行判断地点是否合格
             var latitudeBig = this.data.latitudeBig;
             var latitudeSmall = this.data.latitudeSmall;
             var longitudeBig = this.data.longitudeBig;
             var longitudeSmall = this.data.longitudeSmall;
             var latitude = this.data.latitude;
             var longitude = this.data.longitude;
             var placeName = this.data.placeName;
             var that = this;
             //这里进行判断地点是否合格
             if (latitude != null && longitude != null &&(latitude >= latitudeSmall && latitude <= latitudeBig) && (longitude >= longitudeSmall && longitude <= longitudeBig)) {
               wx.showLoading({
                 title: '正在开始',
                 mask: true
               })
               wx.request({
                 url: url + "/save_studen_begin_time_detail",
                 data: {
                   studentId: app.appData.student_info.id, //学生的id
                   placeId: that.data.placeId //地点
                 },
                 success: function(res) {
                   wx.hideLoading();
                   if (res.data.code == 200) {
                     wx.showModal({
                       title: "提醒",
                       content: "开始成功,记得回来签到",
                       showCancel: false
                     })
                     that.setData({
                       existUnfinishSport: !existUnfinishSport,
                       timeSmall: res.data.timeSmall,
                       timeBig: res.data.timeBig,
                       timeSmallShow: res.data.timeSmallShow,
                       timeBigShow: res.data.timeBigShow
                     })

                   } else {
                     wx.showToast({
                       title: '系统出错了',
                       icon: "none",
                       duration: 3000
                     })
                   }
                 },
                 fail: function(res) {
                   wx.hideLoading();
                   wx.showToast({
                     title: '连接不上服务器',
                     icon: "none",
                     duration: 3000
                   })
                   return;
                 }
               })
             } else {
               wx.showModal({
                 title: 'GPS警告',
                 content: "请前往" + placeName + "地点在开始运动",
                 showCancel: false
               })
               return;
             }



           },

           /**
            * 生命周期函数--监听页面加载
            */
           onLoad: function(options) {
             this.getLocation(); //获取GPS地点
             var that = this;
             //common是自己写的公共JS方法，可忽略
             for (var i = 0; i < 1; i++) {
               this.data.items.push({
                 isTouchMove: false //默认隐藏删除
               })
             }
             this.setData({
               items: this.data.items
             });



             wx.setNavigationBarTitle({ //改变导航栏标题
               title: options.sportType,
             })
             var existUnfinishSport = options.existUnfinishSport === "false" ? false : true;
             this.setData({ //把上个页面传递的值在本页面赋值
               placeId: options.placeId,
               existUnfinishSport: existUnfinishSport,
               latitudeBig: options.latitudeBig,
               latitudeSmall: options.latitudeSmall,
               longitudeBig: options.longitudeBig,
               longitudeSmall: options.longitudeSmall,
               placeName: options.placeName,
             })
             if (options.timeSmall != null && options.timeBig != null) {
               this.setData({
                 timeSmall: options.timeSmall,
                 timeBig: options.timeBig,
                 timeSmallShow: options.timeSmallShow,
                 timeBigShow: options.timeBigShow
               })
             }

           },




   changeParentData: function () {
     var pages = getCurrentPages(); //当前页面栈
     if (pages.length > 1) { //切记本页面不能是redirectTo
       var beforePage = pages[pages.length - 3]; //获取上上一个页面实例对象
       beforePage.changeData(); //触发父页面中的方法
     }
   },

   //手指触摸动作开始 记录起点X坐标
   touchstart: function (e) {
     //开始触摸时 重置所有删除
     this.data.items.forEach(function (v, i) {
       if (v.isTouchMove)//只操作为true的
         v.isTouchMove = false;
     })
     this.setData({
       startX: e.changedTouches[0].clientX,
       startY: e.changedTouches[0].clientY,
       items: this.data.items
     })
   },

   //滑动事件处理

   touchmove: function (e) {
     var that = this,
       index = e.currentTarget.dataset.index,//当前索引
       startX = that.data.startX,//开始X坐标
       startY = that.data.startY,//开始Y坐标
       touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
       touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
       //获取滑动角度
       angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
     that.data.items.forEach(function (v, i) {
       v.isTouchMove = false
       //滑动超过50度角 return
       if (Math.abs(angle) > 50) return;
       if (i == index) {
         if (touchMoveX > startX) //右滑
           v.isTouchMove = false
         else //左滑
           v.isTouchMove = true
       }
     })
     //更新数据
     that.setData({
       items: that.data.items
     })
   },

   /**
  
  * 计算滑动角度
  
  * @param {Object} start 起点坐标
  
  * @param {Object} end 终点坐标
  
  */
   angle: function (start, end) {
     var _X = end.X - start.X,
       _Y = end.Y - start.Y
     //返回角度 /Math.atan()返回数字的反正切值
     return 360 * Math.atan(_Y / _X) / (2 * Math.PI);

   },
           /**
            * 生命周期函数--监听页面初次渲染完成
            */
           onReady: function() {

           },

           /**
            * 生命周期函数--监听页面显示
            */
           onShow: function() {

           },

           /**
            * 生命周期函数--监听页面隐藏
            */
           onHide: function() {

           },

           /**
            * 生命周期函数--监听页面卸载
            */
           onUnload: function() {

           },

           /**
            * 页面相关事件处理函数--监听用户下拉动作
            */
           onPullDownRefresh: function() {

           },

           /**
            * 页面上拉触底事件的处理函数
            */
           onReachBottom: function() {

           },

           /**
            * 用户点击右上角分享
            */
           onShareAppMessage: function() {

           }
       })