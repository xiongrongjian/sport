// pages/index/content/content.js
const app = getApp();
var url = app.appData.url //获取地址
Page({

  /**
   * 页面的初始数据
   */
  data: {
    content:null,
  },



  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var id = options.id;
    var that=this;
    
    wx.request({
      url: url +'/get_one_content_by_informtion_id',
      data: {
        id: id,
      },
      success: function(res) {
        if(res.data.code==200){
        
          that.setData({
            content:res.data.data
          })
        }else{
          wx.showToast({
            title: '信息已经被删',
            icon: "none"
          })
          return;
        }
       
      },
      fail: function() {
        wx.showToast({
          title: '服务器出错了',
          icon: "none"
        })
        return;
      }
    })
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})