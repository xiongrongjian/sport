// pages/person/logout/logout.js
const app = getApp();
var url = app.appData.url //获取地址
Page({

  /**
   * 页面的初始数据
   */
  data: {
    student_id: app.appData.student_info.id
  },


  logout:function(){
    wx.showModal({
      title: '提示',
      content: '为防止作弊注销将会删除本次开始签到,你确定?',
       success:function(res){
         if(res.confirm){
           wx.showLoading({
             title: '加载中',
           });
           wx.request({
             url: url +"/logout_and_remove_redis_begintime",
             data: {
               student_id: app.appData.student_info.id  },
               success:function(res){
                 wx.hideLoading();
                 if(res.data.code==200){
                   wx.removeStorage({
                     key: 'student_info_account_password',
                     success: function(res) {
                       wx.redirectTo({
                         url: '../../login/login',
                       })
                     },
                   })
                 }
                 else{
                   wx.showToast({
                     title: '出错了',
                     icon:"none",
                     mask:true,
                     duration:3000
                   })
                 }
               },
               faile:function(){
                 wx.showToast({
                   title: '服务器出错了',
                   mask: true,
                   duration: 3000
                 })
               }
           })
         }
       }
    })
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})