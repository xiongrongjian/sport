/**
 * 2018.10.14
 * 熊荣健制作
 */
App({ 

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function() {
 

   
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function(options) {

  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function() {

  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function(msg) {

  },


  globalData: {
    userInfo: null
  },

  //全局数据
  appData: {
  
    url: "http://120.79.246.198:8082",//120.79.246.198:8082,注意阿里云策略组端口
    student_info: {}                   //127.0.0.1:8080
  }


})